/*Basic functionality scripts */

var jsTest;

function loadTests() {
	$.ajax({
		dataType: "json",
		url: "rest/tests",
		data: {"token": window.authToken},
		success: function (json) {
			$("#testsTable tbody").append(printTests(json));
			$("#testsTable").trigger("update");
		}
	});
}

function printTests(arg) {
	var output = '';
	for (var i in arg) {
		output += '<tr><td>' + '<a href="pass_test.html?id=' + arg[i].test_id + '">' + arg[i].title + '</a></td><td>' + arg[i].author + '</td><td>' + arg[i].test_category + '</td><td>' + arg[i].date + '</td></tr>';
	}
	return output;
}

function loadQuestions() {
	$.ajax({
		dataType: "json",
		url: "rest/tests/test?id=" + getParamValue('id'),
		data: {"token": window.authToken},
		success: function (json) {
			jsTest = json;
			$("#questionsPanel").append(printQuestions(json));
			printDescription(json);
		}
	});
}

function printQuestions(arg) {
	var output = '';
	for (var t = 0; t < arg.questions.length; t++) {
		for (var i = 0; i < arg.questions.length; i++) {
			if (arg.questions[i].number == t) {
				if (arg.questions[i].type == 1) {
					output += '<div class="panel panel-default"><div class="panel-heading">Question ' + (arg.questions[i].number + 1) + ': ' + arg.questions[i].title + ' </div><div class="panel-body">';
					for (var n in arg.questions[i].answers) {
						output += '<div class="radio"><label><input type="radio" name="optradio' + (arg.questions[i].number + 1) + '" value=' + arg.questions[i].answers[n].number + ' /> ' + arg.questions[i].answers[n].title + '</label></div>';
					}
					output += '</div></div>';
				} else if (arg.questions[i].type == 2) {
					output += '<div class="panel panel-default"><div class="panel-heading">Question ' + (arg.questions[i].number + 1) + ': ' + arg.questions[i].title + ' </div><div class="panel-body">';
					for (var n in arg.questions[i].answers) {
						output += '<div class="checkbox"><label><input type="checkbox" name="optchkbx' + (arg.questions[i].number + 1) + '" value=' + arg.questions[i].answers[n].number + ' /> ' + arg.questions[i].answers[n].title + '</label></div>';
					}
					output += '</div></div>';
				} else if (arg.questions[i].type == 3) {
					output += '<div class="panel panel-default"><div class="panel-heading">Question ' + (arg.questions[i].number + 1) + ': ' + arg.questions[i].title + ' </div><div class="panel-body">';
					output += '<label><input type="text" class="form-control" name="opttext' + (arg.questions[i].number + 1) + '" > ' + '</label>';
					output += '</div></div>';
				}
			}
		}
	}
	return output;
}

function printDescription(arg) {
	$("#testTitle").html(arg.title);
	$("#testDesc").html(arg.description);
}

function sendTestAnswers() {
	if (checkRadios()) {
		var data = populateData();
		$.ajax({
			url: 'rest/tests/passed_test',
			type: 'POST',
			data: JSON.stringify(data),
			contentType: 'application/json; charset=utf-8',
			success: function (response) {
				if (response.result == 200) {
					var rating = Math.round(response.userWeight / response.totalWeight * 100);
					$("#submitBtn").hide();
					$("#resultPanel").append("Your rating is: " + rating + "%").css("visibility", "visible");
					alert("You got " + response.userWeight + "/" + response.totalWeight + " answers right!");
				}
			}
		});
	}
}

function populateData() {
	var answersTest = new Object();
	answersTest.test_id = jsTest.test_id;
	answersTest.token = window.authToken;
	answersTest.questions = [];
	var radioName = "";
	for (var i = 0; i < jsTest.questions.length; i++) {
		answersTest.questions[i] = new Object();
		answersTest.questions[i].number = jsTest.questions[i].number;
		answersTest.questions[i].title = jsTest.questions[i].title;
		if (jsTest.questions[i].type == 1) {
			radioName = "input:radio[name=optradio" + (jsTest.questions[i].number + 1) + "]:checked";
			answersTest.questions[i].answer = $(radioName).val();
		} else if (jsTest.questions[i].type == 2) {
			var yourArray = [];
			radioName = "input:checkbox[name=optchkbx" + (jsTest.questions[i].number + 1) + "]:checked";
			$(radioName).each(function () {
				yourArray.push($(this).val());
			});
			answersTest.questions[i].answer = yourArray.toString();
		} else if (jsTest.questions[i].type == 3) {
			radioName = "input:text[name=opttext" + (jsTest.questions[i].number + 1) + "]";
			answersTest.questions[i].answer = $(radioName).val();
		}
	}
	return answersTest;
}

function checkRadios() {
	for (var i = 0; i < jsTest.questions.length; i++) {
		if (jsTest.questions[i].type == 1) {
			if (!$("input[name='optradio" + (jsTest.questions[i].number + 1) + "']:checked").val()) {
				alert("Please, answer question number " + (jsTest.questions[i].number + 1));
				return false;
			}
		} else if (jsTest.questions[i].type == 3) {
			if (!$("input[name='opttext" + (jsTest.questions[i].number + 1) + "']").val()) {
				alert("Please, answer question number " + (jsTest.questions[i].number + 1));
				return false;
			}
		}
	}
	return true;
}

function getParamValue(name) {
	var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
	if (results == null) {
		return null;
	}
	else {
		return results[1] || 0;
	}
}

function validateEmail() {
	var regexp = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/i;
	if (regexp.test($('#inputEmail').val())) {
		return true;
	}
	else {
		return false;
	}
}

function validateLogin() {
	var regx = /^[a-zA-Z0-9_]+$/i;
	if (regx.test($('#inputEmail').val()) || validateEmail()) {
		$("#emailHelp").html("");
		$('#emailVal').removeClass("has-error");
		return true;
	}
	else {
		$("#emailHelp").html("Enter a valid login or email!");
		$('#emailVal').addClass("has-error");
		return false;
	}
}

function validatePassword() {
	var regx = /^[a-zA-Z0-9_]+$/i;
	if ($('#inputPassword').val().length != 0 && regx.test($('#inputPassword').val())) {
		$("#passHelp").html("");
		$('#passVal').removeClass("has-error");
		return true;
	}
	else {
		$("#passHelp").html("Enter a password!");
		$('#passVal').addClass("has-error");
		return false;
	}
}

function validateReg() {
	var regx = /^[a-zA-Z0-9_]+$/i;
	var regexp = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/i;

	if (!regx.test($('#inputUsernameReg').val())) {
		$('#unregVal').addClass("has-error");
		return false;
	} else {
		$('#unregVal').removeClass("has-error")
	}
	if (!regexp.test($('#inputEmailReg').val())) {
		$('#emailregVal').addClass("has-error");
		return false;
	} else {
		$('#emailregVal').removeClass("has-error")
	}
	if (!regx.test($('#inputPasswordReg').val()) || !regx.test($('#inputPasswordRegRepeat').val()) || !($('#inputPasswordReg').val() == $('#inputPasswordRegRepeat').val())) {
		$('#passregVal').addClass("has-error");
		return false;
	} else {
		$('#passregVal').removeClass("has-error")
	}
	return true;
}

function auth(username, hash) {
	$.ajax({
		url: 'rest/auth/login',
		type: "POST",
		data: JSON.stringify({"userName": username, "passHash": md5(hash)}),
		dataType: "json",
		contentType: 'application/json; charset=utf-8',
		success: function (response) {
			if (response.result == 255 || response.result == 250) {
				$("#emailHelp").html("Wrong login, e-mail or password!");
				$('#emailVal').addClass("has-error");
			} else if (typeof response.token != 'undefined') {
				document.cookie = "authToken=" + response.token;
				if ($("input:checkbox[name=remMe]:checked").val()) {
					var date = new Date();
					date = new Date(date.getTime() + 1000*60*60*24*5); //5 days cookie
					document.cookie += ";expires=" + date.toUTCString();
				}
				window.location.href = 'menu.html';
			}
		}
	});
}

function getCookie(cname) {
	var name = cname + "=";
	var ca = document.cookie.split(';');
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') c = c.substring(1);
		if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
	}
}

function init() {
	var token = getCookie("authToken");
	if (token != undefined)
		window.authToken = token;
	if (window.authToken == undefined)
		window.location.href = 'index.html';
}

function logIn() {
	if (validateLogin() && validatePassword()) {
		auth($("#inputEmail").val(), $("#inputPassword").val());
	}
}

function logInGuest() {
	$.ajax({
		url: 'rest/auth/anonymous',
		type: "POST",
		data: JSON.stringify({"userName": "anonymous"}),
		dataType: "json",
		contentType: 'application/json; charset=utf-8',
		success: function (response) {
			document.cookie = "authToken=" + response.token;
			window.location.href = 'menu.html';
		}
	});
}

function signUp() {
	if (validateReg()) {
		$.ajax({
			url: 'rest/auth/signup',
			type: "POST",
			data: JSON.stringify({
				"userName": $("#inputUsernameReg").val(),
				"passHash": md5($("#inputPasswordReg").val()),
				"email": $("#inputEmailReg").val()
			}),
			dataType: "json",
			contentType: 'application/json; charset=utf-8',
			success: function (response) {
				if (response.result == 205) {
					$('#unregVal').addClass("has-error");
					$("#regErrors").html("Username already in use!");
				} else if (response.result == 210) {
					$("#regErrors").html("Email already in use!");
					$('#emailregVal').addClass("has-error");
				} else if (response.result == 200) {
					$("#regErrors").html("Registration complete!");
				}
			}
		});
	}
}

function logOut() {
	$.ajax({
		url: 'rest/auth/logout',
		type: "POST",
		data: JSON.stringify({"token": window.authToken}),
		dataType: "json",
		contentType: 'application/json; charset=utf-8',
		success: function () {
			document.cookie = 'authToken=; Max-Age=0';
			window.location.href = 'index.html';
		}
	});
}

function getRandomTest() {
	$.ajax({
		dataType: "json",
		url: "rest/tests/random_id",
		success: function (randomId) {
			window.location.href = 'pass_test.html?id=' + randomId.test_id;
		}
	});
}

function loadPassedTests() {
	$.ajax({
		dataType: "json",
		url: "rest/cabinet/passed_tests",
		type: "POST",
		data: JSON.stringify({"token": window.authToken}),
		success: function (json) {
			$("#testsTable tbody").append(printPassedTests(json));
			$("#testsTable").trigger("update");
		}
	});
}

function printPassedTests(arg) {
	var output = '';
	for (var i in arg.tests) {
		output += '<tr><td>' + '<a href="test_results.html?id=' + arg.tests[i].test_id + '">' + arg.tests[i].title + '</a></td><td>' + arg.tests[i].date + '</td><td>' + arg.tests[i].userWeight + '/' + arg.tests[i].totalWeight + '</td><td>' + (Math.round(arg.tests[i].userWeight / arg.tests[i].totalWeight * 100)) + '</td></tr>';
	}
	return output;
}

function loadCreatedTests() {
	$.ajax({
		dataType: "json",
		url: "rest/cabinet/created_tests",
		type: "POST",
		data: JSON.stringify({"token": window.authToken}),
		success: function (json) {
			$("#testsTable tbody").append(printCreatedTests(json));
			$("#testsTable").trigger("update");
		}
	});
}

function printCreatedTests(arg) {
	var output = '';
	for (var i in arg.tests) {
		output += '<tr><td>' + arg.tests[i].title + '</td><td>' + arg.tests[i].date + '</td><td>' + arg.tests[i].count + '</td></tr>';
	}
	return output;
}

function loadQuestionsResult() {
	$.ajax({
		dataType: "json",
		url: "rest/cabinet/test_in_details",
		type: "POST",
		data: JSON.stringify({"token": window.authToken, "test_id": parseInt(getParamValue('id'))}),
		success: function (json) {
			jsTest = json;
			printDescription(json);
			$("#questionsPanel").append(printQuestionsResult(json));
			var rating = Math.round(json.userWeight / json.totalWeight * 100);
			$("#resultPanel").append("Your rating is: " + rating + "%").css("visibility", "visible");

		}
	});
}

function printQuestionsResult(arg) {
	var output = '';
	for (var t = 0; t < arg.questions.length; t++) {
		for (var i = 0; i < arg.questions.length; i++) {
			if (arg.questions[i].number == t) {
				if (arg.questions[i].type == 1) {
					output += '<div class="panel panel-default"><div class="panel-heading">Question ' + (arg.questions[i].number + 1) + ': ' + arg.questions[i].title + ' </div><div class="panel-body">';
					for (var n in arg.questions[i].answers) {
						output += '<div class="radio"><label><input type="radio" name="optradio' + (arg.questions[i].number + 1) + '" value=' + arg.questions[i].answers[n].number;
						if (arg.questions[i].answers[n].choosed == 1) {
							if (arg.questions[i].answers[n].right == 1) {
								var headerClass = "greenHead";
							} else {
								var headerClass = "redHead";
							}
							var tmp = output.slice(0, output.lastIndexOf("panel-heading") + 13) + " " + headerClass + output.slice(output.lastIndexOf("panel-heading") + 13, output.length);
							output = tmp;
							output += ' checked="checked"/>';
						} else {
							output += '/>';
						}
						output += arg.questions[i].answers[n].title + '</label></div>';
					}
					output += '</div></div>';
				} else if (arg.questions[i].type == 2) {
					var colorTrue = true;
					output += '<div class="panel panel-default"><div class="panel-heading">Question ' + (arg.questions[i].number + 1) + ': ' + arg.questions[i].title + ' </div><div class="panel-body">';
					for (var n in arg.questions[i].answers) {
						output += '<div class="checkbox"><label><input type="checkbox" name="optchkbx' + (arg.questions[i].number + 1) + '" value=' + arg.questions[i].answers[n].number;
						if (arg.questions[i].answers[n].choosed == 1) {
							if (arg.questions[i].answers[n].right == 1 && colorTrue) {
								var headerClass = "greenHead";
							}
							else {
								var headerClass = "redHead";
							}
							var tmp = output.slice(0, output.lastIndexOf("panel-heading") + 13) + " " + headerClass + output.slice(output.lastIndexOf("panel-heading") + 13, output.length);
							output = tmp;
							colorTrue = false;
							output += ' checked="checked"/>';
						} else {
							output += '/>';
						}
						output += arg.questions[i].answers[n].title + '</label></div>';
					}
					output += '</div></div>';
				} else if (arg.questions[i].type == 3) {
					if (arg.questions[i].is_right == 1) {
						output += '<div class="panel panel-default"><div class="panel-heading greenHead">';
					} else {
						output += '<div class="panel panel-default"><div class="panel-heading redHead">';
					}
					output += 'Question ' + (arg.questions[i].number + 1) + ': ' + arg.questions[i].title + ' </div><div class="panel-body">';
					output += '<label><input type="text" class="form-control" name="opttext' + (arg.questions[i].number + 1) + '" value="' + arg.questions[i].user_answer + '"> ' + '</label>';
					output += '</div></div>';
				}
			}
		}
	}
	return output;
}

function anonymize() {
	$("#plsLogin").hide();
	var login = "";
	for (var i = 1; i < window.authToken.length; i += 2) {
		login += window.authToken.charAt(i);
	}
	if (login=="anonymous") {
		$(".anonBtn").hide();
		$("#plsLogin").show();
	}
}

function getPlotDataPC() {
	$.ajax({
		url: 'rest/cabinet/passed_created',
		type: "POST",
		data: JSON.stringify({"token": window.authToken}),
		dataType: "json",
		contentType: 'application/json; charset=utf-8',
		success: function (resp) {
			if (!(resp.result == 155)) {
				plotPersonal(resp);
			}
		}
	});
}

function plotPersonal(data) {
	var series = [];
	var series2 = [];
	if (data.created != 0) {
		for (var i in data.created) {
			series[i] = [];
			series[i][0] = (new Date(data.created[i].date).getTime());
			series[i][1] = data.created[i].count;
		}
		series.sort();
	}
	if (data.passed != 0) {
		for (var i in data.passed) {
			series2[i] = [];
			series2[i][0] = (new Date(data.passed[i].date).getTime());
			series2[i][1] = data.passed[i].count;
		}
		series2.sort();
	}
	$.plot($("#placeholder"), [{label: "Created tests", data: series},
		{label: "Passed tests", data: series2}], {
		xaxis: {
			mode: "time",
			timeformat: "%Y-%m-%d"
		},
		yaxis: {min: 0},
		lines: {show: true},
		points: {show: true}
	});
}
