package com.lilsmile;

import db.DBContorller;
import db.DBControllerMethods;
import db.entity.Answer;
import db.entity.Question;
import db.entity.Test;
import db.entity.UserPass;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Path("/cabinet")
public class PersonalCabinet implements Constants {

    DBControllerMethods dbController = new DBContorller();

    @POST
    @Path("/passed_created")
    @Consumes(MediaType.APPLICATION_JSON)
    public String getPassedAndCreatedTests(String body)
    {
        JSONObject jsonObject = (JSONObject) JSONValue.parse(body);
        String token = (String) jsonObject.get(TOKEN);
        if (!StaticThings.checkToken(token))
        {
            StaticThings.writeInfo("tryna login with wrong token");
            JSONObject request = new JSONObject();
            request.put(RESULT, WRONG_TOKEN);
            return request.toString();
        }
        String login = StaticThings.loginFromToken(token);
        try {
            List<UserPass> passedTests = dbController.getPassedTestsByUser(dbController.getUserByLogin(login));
            List<Test> createdTests = dbController.getTestsCreatedByUser(dbController.getUserByLogin(login));
            JSONObject request = new JSONObject();
            JSONArray arrayCreated = new JSONArray();
            JSONArray arrayPassed = new JSONArray();
            ArrayList<CountAndDate> array = new ArrayList<CountAndDate>();
            if (passedTests==null)
            {
                request.put(PASSED, "0");
            } else {
                for (UserPass userPass : passedTests) {
                    Date tmpDate = userPass.getDate();
                    boolean flag = false;
                    for (int i = 0; i < array.size(); i++) {
                        if (array.get(i).date.equals(tmpDate)) {
                            flag = true;
                            array.get(i).count++;
                            break;
                        }
                    }
                    if (!flag) {
                        CountAndDate countAndDate = new CountAndDate(tmpDate);
                        array.add(countAndDate);
                    }
                }
                for (CountAndDate countAndDate : array) {
                    JSONObject tmp = new JSONObject();
                    tmp.put(COUNT, countAndDate.count);
                    tmp.put(DATE, countAndDate.dateString);
                    arrayPassed.add(tmp);
                }
                request.put(PASSED, arrayPassed);
            }
            if (createdTests==null)
            {
                request.put(CREATED, "0");
            } else {
                array = new ArrayList<CountAndDate>();
                for (Test test : createdTests) {
                    Date tmpDate = test.getDate();
                    boolean flag = false;
                    for (int i = 0; i < array.size(); i++) {
                        if (array.get(i).date.equals(tmpDate)) {
                            flag = true;
                            array.get(i).count++;
                            break;
                        }
                    }
                    if (!flag) {
                        CountAndDate countAndDate = new CountAndDate(tmpDate);
                        array.add(countAndDate);
                    }
                }
                for (CountAndDate countAndDate : array) {
                    JSONObject tmp = new JSONObject();
                    tmp.put(COUNT, countAndDate.count);
                    tmp.put(DATE, countAndDate.dateString);
                    arrayCreated.add(tmp);
                }
                request.put(CREATED, arrayCreated);
            }
            return request.toString();
        } catch (SQLException e) {
            //e.printStackTrace();
            StaticThings.writeInfo(e.getMessage());
            JSONObject response = new JSONObject();
            response.put(RESULT, SMTH_IS_WRONG);
            return response.toString();
        }
    }

    @POST
    @Path("/created_tests")
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllCreatedTests(String body)
    {
        JSONObject request = (JSONObject) JSONValue.parse(body);
        String token = (String) request.get(TOKEN);
        if (!StaticThings.checkToken(token))
        {
            StaticThings.writeInfo("got new test with wrong token");
            JSONObject jsonObject1 = new JSONObject();
            jsonObject1.put(RESULT, WRONG_TOKEN);
            return jsonObject1.toString();
        }

        String login = StaticThings.loginFromToken(token);
        try {
            List<Test> tests = dbController.getTestsCreatedByUser(dbController.getUserByLogin(login));
            JSONObject response = new JSONObject();
            response.put(LOGIN, login);
            JSONArray arrayTests = new JSONArray();
            for (Test test: tests)
            {
                List<UserPass> list = dbController.getPassesOfTheTestById(test.getIdTest());
                int count;
                if (list==null)
                {
                    count=0;
                } else
                {
                    count = list.size();
                }
                JSONObject currentTest = new JSONObject();
                currentTest.put(TEST_ID, test.getIdTest());
                currentTest.put(TEST_CATEGORY, test.getTestCategory().name().toLowerCase());
                Date date = test.getDate();
                int year = date.getYear()+1900;
                int month = date.getMonth()+1;
                int day = date.getDate();
                StringBuilder sb = new StringBuilder();
                sb.append(year).append("-");
                if (month<10)
                {
                    sb.append("0");
                }
                sb.append(month).append("-");
                if (day<10)
                {
                    sb.append(0);
                }
                sb.append(day);
                currentTest.put(DATE, sb.toString());
                currentTest.put(DESCRIPTION, test.getDescription());
                currentTest.put(TITLE, test.getTitle());
                currentTest.put(COUNT, count);
                arrayTests.add(currentTest);
            }
            response.put(TESTS, arrayTests);
            return response.toString();
        } catch (SQLException e) {
            StaticThings.writeInfo(e.getMessage());
            JSONObject jsonObject1 = new JSONObject();
            jsonObject1.put(RESULT, SMTH_IS_WRONG);
            return jsonObject1.toString();
        }

    }

    @POST
    @Path("/passed_tests")
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllPassedTests(String body)
    {
        JSONObject request = (JSONObject) JSONValue.parse(body);
        String token = (String) request.get(TOKEN);
        if (!StaticThings.checkToken(token))
        {
            StaticThings.writeInfo("got new test with wrong token");
            JSONObject jsonObject1 = new JSONObject();
            jsonObject1.put(RESULT, WRONG_TOKEN);
            return jsonObject1.toString();
        }

        String login = StaticThings.loginFromToken(token);
        try {
            List<UserPass> tests = dbController.getPassedTestsByUser(dbController.getUserByLogin(login));
            JSONObject response = new JSONObject();
            response.put(LOGIN, login);
            JSONArray arrayTests = new JSONArray();
            for (UserPass test: tests)
            {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put(TEST_ID, test.getId());
                jsonObject.put(AUTHOR, test.getTest().getAuthor().getLogin());
                jsonObject.put(TITLE, test.getTest().getTitle());
                jsonObject.put(TEST_CATEGORY, test.getTest().getTestCategory().name().toLowerCase());
                Date date = test.getDate();
                int year = date.getYear()+1900;
                int month = date.getMonth()+1;
                int day = date.getDate();
                StringBuilder sb = new StringBuilder();
                sb.append(year).append("-");
                if (month<10)
                {
                    sb.append("0");
                }
                sb.append(month).append("-");
                if (day<10)
                {
                    sb.append(0);
                }
                sb.append(day);
                jsonObject.put(DATE, sb.toString());
                jsonObject.put(DESCRIPTION, test.getTest().getDescription());
                jsonObject.put(TOTAL_WEIGHT, test.getTest().getMaxWeight());
                jsonObject.put(USER_WEIGHT, test.getUserWeight());
                arrayTests.add(jsonObject);
            }
            response.put(TESTS, arrayTests);
            return response.toString();
        } catch (SQLException e) {
            StaticThings.writeInfo(e.getMessage());
            JSONObject jsonObject1 = new JSONObject();
            jsonObject1.put(RESULT, SMTH_IS_WRONG);
            return jsonObject1.toString();
        }
    }

    @POST
    @Path("/test_in_details")
    @Produces(MediaType.APPLICATION_JSON)
    public String getTestInDetail(String body) {
        JSONObject request = (JSONObject) JSONValue.parse(body);
        String token = (String) request.get(TOKEN);
        if (!StaticThings.checkToken(token)) {
            StaticThings.writeInfo("got new test with wrong token");
            JSONObject jsonObject1 = new JSONObject();
            jsonObject1.put(RESULT, WRONG_TOKEN);
            return jsonObject1.toString();
        }

        String login = StaticThings.loginFromToken(token);
        int testID = ((Long) request.get(TEST_ID)).intValue();

        try {
            Test test = dbController.getPassedTest(testID);
            JSONObject response = new JSONObject();
            response.put(TITLE, test.getTitle());
            response.put(TEST_CATEGORY, test.getTestCategory().toString());
            response.put(DESCRIPTION, test.getDescription());
            response.put(TOTAL_WEIGHT, test.getMaxWeight());
            response.put(USER_WEIGHT, test.getUserWeight());
            JSONArray questionsArray = new JSONArray();
            for (Question question : test.getQuestions())
            {
                JSONObject currentQuestion = new JSONObject();
                currentQuestion.put(TITLE, question.getTitle());
                currentQuestion.put(TYPE, question.getQuestionType());
                currentQuestion.put(NUMBER, question.getNumber());
                if (question.getQuestionType()==3)
                {
                    String userAnswer = question.getAnswerText();
                    String answer = question.getAnswers().iterator().next().getTitle();
                    String[] answers = answer.split(",");
                    boolean flag = false;
                    for (int i = 0; i< answers.length; i++)
                    {
                        if (userAnswer.trim().toLowerCase().equals(answers[i].trim().toLowerCase()))
                        {
                            flag = true;
                            break;
                        }
                    }
                    currentQuestion.put(IS_RIGHT, flag ? "1":"0");
                    currentQuestion.put(USER_ANSWER, question.getAnswerText());
                    currentQuestion.put(ANSWER, question.getAnswers().iterator().next().getTitle());
                } else
                {
                    JSONArray answersArray = new JSONArray();
                    for (Answer answer : question.getAnswers())
                    {
                        JSONObject currentAnswer = new JSONObject();
                        currentAnswer.put(NUMBER, answer.getNumber());
                        currentAnswer.put(TITLE, answer.getTitle());
                        currentAnswer.put(RIGHT, answer.getWeight()>0 ? 1:0);
                        currentAnswer.put(CHOOSED, answer.getIsChoosed());
                        answersArray.add(currentAnswer);
                    }
                    currentQuestion.put(ANSWERS, answersArray);
                }
                questionsArray.add(currentQuestion);
            }
            response.put(QUESTIONS, questionsArray);
            return response.toString();
        } catch (SQLException e) {
            StaticThings.writeInfo(e.getMessage());
            JSONObject jsonObject1 = new JSONObject();
            jsonObject1.put(RESULT, SMTH_IS_WRONG);
            return jsonObject1.toString();
        }
    }




    private class CountAndDate
    {
        public int count;
        public String dateString;
        public Date date;

        CountAndDate(Date date)
        {
            this.date = date;
            int year = date.getYear()+1900;
            int month = date.getMonth()+1;
            int day = date.getDate();
            StringBuilder sb = new StringBuilder();
            sb.append(year).append("-");
            if (month<10)
            {
                sb.append("0");
            }
            sb.append(month).append("-");
            if (day<10)
            {
                sb.append(0);
            }
            sb.append(day);
            this.dateString=sb.toString();
            this.count=1;
        }

    }


}
