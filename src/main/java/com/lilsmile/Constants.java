package com.lilsmile;


public interface Constants {


    String TESTS = "tests";
    String TEST_ID = "test_id";
    String AUTHOR = "author";
    String TEST_CATEGORY = "test_category";
    String TITLE = "title";
    String DESCRIPTION = "description";
    String DATE = "date";
    String QUESTIONS = "questions";
    String ANSWERS = "answers";
    String ANSWER = "answer";
    String USER_ANSWER = "user_answer";
    String NUMBER = "number";
    String WEIGHT = "weight";
    String TOKEN = "token";
    String TYPE = "type";
    String ANSWERS_ARR="answersArr";
    String COUNT = "count";
    String RIGHT = "right";
    String CHOOSED = "choosed";
    String IS_RIGHT = "is_right";

    String TOTAL_WEIGHT="totalWeight";
    String USER_WEIGHT="userWeight";

    String CREATED = "created";
    String PASSED = "passed";

    //codes for signup
    String OK="200";
    String BAD_LOGIN="205";
    String BAD_EMAIL="210";
    //codes for login
    String WRONG_PASSWORD="250";
    String WRONG_LOGIN="255";

    String LOGIN = "userName";
    String PASSWORD = "passHash";
    String EMAIL = "email";

    String RESULT="result";

    //errors
    String SMTH_IS_WRONG = "100";
    String WRONG_TOKEN = "155";
}
